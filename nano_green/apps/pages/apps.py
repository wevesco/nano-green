from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = 'nano_green.apps.home'
