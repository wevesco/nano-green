from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'nano_green.apps.blog'
