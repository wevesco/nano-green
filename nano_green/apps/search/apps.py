from django.apps import AppConfig


class SearchConfig(AppConfig):
    name = 'nano_green.apps.search'
