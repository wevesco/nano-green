from django import forms


class Step1Selection(forms.Form):
    selection = forms.ChoiceField(
        choices=(
            ('vyfotit', 'Vyfotit vyúčtování'),
            ('vyplnit', 'Vyplnit formulář'),
        )
    )


class Step2Scan(forms.Form):
    # TODO company.
    first_name = forms.CharField()
    last_name = forms.CharField()
    phone = forms.CharField()
    email = forms.EmailField()
    birth_date = forms.DateField()
    address_differs = forms.BooleanField()
    invoice = forms.ImageField()


class Step2Person(forms.Form):
    # TODO company.
    first_name = forms.CharField()
    last_name = forms.CharField()
    phone = forms.CharField()
    email = forms.EmailField()
    birth_date = forms.DateField()


class Step3Place(forms.Form):
    moving = forms.BooleanField()
    street = forms.CharField()
    house_number = forms.CharField()
    city = forms.CharField()
    zip_code = forms.CharField()
    different_postal_address = forms.BooleanField()
    different_residence_address = forms.BooleanField()


class Step4Invoice(forms.Form):
    # TODO remaining fields
    ean = forms.CharField()
    current_supplier = forms.CharField()


class Step5Payment(forms.Form):
    # TODO remaining fields
    monthly_advance = forms.IntegerField()


class Step6Summary(forms.Form):
    pass
