from django.apps import AppConfig


class OrderConfig(AppConfig):
    name = 'nano_green.apps.order'
