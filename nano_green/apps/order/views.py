import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from formtools.wizard.views import NamedUrlCookieWizardView

from nano_green.apps.order import forms


FORMS = [
    ('selection', forms.Step1Selection),
    ('scan', forms.Step2Scan),
    ('person', forms.Step2Person),
    ('place', forms.Step3Place),
    ('invoice', forms.Step4Invoice),
    ('payment', forms.Step5Payment),
    ('summary', forms.Step6Summary),
]

TEMPLATES = {
    'selection': 'wizard/step.html',
    'scan': 'wizard/step.html',
    'person': 'wizard/step.html',
    'place': 'wizard/step.html',
    'invoice': 'wizard/step.html',
    'payment': 'wizard/step.html',
    'summary': 'wizard/summary.html',
}


class OrderWizard(NamedUrlCookieWizardView):
    file_storage = FileSystemStorage(
        location=os.path.join(settings.MEDIA_ROOT, 'photos')
    )

    def done(self, form_list, **kwargs):
        return render(self.request, 'wizard/done.html', {
            'form_data': [form.cleaned_data for form in form_list],
        })

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        if self.steps.current == 'summary':
            context.update({'all_data': self.get_all_cleaned_data()})
        return context

    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]
