from django.conf.urls import url

from nano_green.apps.order.views import OrderWizard, FORMS


def scan_selected(wizard):
    # Try to get the cleaned data of selection step.
    cleaned_data = wizard.get_cleaned_data_for_step('selection')

    if not cleaned_data:
        return False

    # Return is scan was selected in first step.
    return cleaned_data['selection'] == 'vyfotit'


def fill_selected(wizard):
    # Try to get the cleaned data of selection step.
    cleaned_data = wizard.get_cleaned_data_for_step('selection')

    if not cleaned_data:
        return False

    # Return is scan was selected in first step.
    return cleaned_data['selection'] == 'vyplnit'


contact_wizard = OrderWizard.as_view(
    FORMS,
    url_name='order:order_step',
    done_step_name='finished',
    condition_dict={
        'scan': scan_selected,
        'person': fill_selected,
        'place': fill_selected,
        'invoice': fill_selected,
        'payment': fill_selected,
    }
)


app_name = 'order'

urlpatterns = [
    url(r'^(?P<step>.+)/$', contact_wizard, name='order_step'),
    url(r'^$', contact_wizard, name='order'),
]
